package org.komenda.project;

import org.komenda.project.printers.Printer;
import org.komenda.project.readers.Reader;

class Replacer {
    private Reader reader;
    private Printer printer;

    Replacer(Reader reader, Printer printer) {
        this.reader = reader;
        this.printer = printer;
    }

    void replace() {
        String string = reader.read();
        string = string.replaceAll(":\\)", ":(");
        printer.print(string);
    }
}

package org.komenda.project.readers;

import java.util.Scanner;

/**
 * Данный класс реализует интерфейс Reader.
 * Предназначен для получения текста со станддартного потока ввода
 *
 * @author Komissarova M.E. 16it18k
 */
public class ConsoleReader implements Reader {

    /**
     * Возвращает строку полученную со станддартного потока ввода
     *
     * @return String строка полученная со станддартного потока ввода
     */
    @Override
    public String read() {
        Scanner reader = new Scanner(System.in);
        return reader.nextLine();
    }
}

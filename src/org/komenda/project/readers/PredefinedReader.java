package org.komenda.project.readers;

public class PredefinedReader implements Reader {
    private String text;

    public PredefinedReader(String text) {
        this.text = text;
    }

    @Override
    public String read() {
        return text;
    }
}

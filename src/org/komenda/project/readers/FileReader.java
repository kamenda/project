package org.komenda.project.readers;

import java.io.BufferedReader;
import java.io.IOException;

public class FileReader implements Reader {
    private String path;

    public  FileReader(String path) {
        this.path = path;
    }

    @Override
    public String read() {
        try(BufferedReader reader = new BufferedReader(new java.io.FileReader(path))) {
            return reader.readLine();
        } catch(IOException ioe) {
            ioe.printStackTrace();
            return null;
        }
    }
}

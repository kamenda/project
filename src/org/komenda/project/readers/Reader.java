package org.komenda.project.readers;

public interface Reader {
    String read();
}

package org.komenda.project;

import org.komenda.project.printers.ConsolePrinter;
import org.komenda.project.printers.Printer;
import org.komenda.project.readers.ConsoleReader;
import org.komenda.project.readers.FileReader;
import org.komenda.project.readers.PredefinedReader;
import org.komenda.project.readers.Reader;

public class Main {
    public static void main(String[] args) {
        Reader predefinedReader = new PredefinedReader(":)");
        Reader fileReader = new FileReader("src//org//komenda//project//in.txt");
        Reader consoleReader = new ConsoleReader();

        Printer printer = new ConsolePrinter();

        Replacer predefinedToConsoleReplacer = new Replacer(predefinedReader, printer);
        Replacer fileToConsoleReplacer = new Replacer(fileReader, printer);
        Replacer consoleOnlyReplacer = new Replacer(consoleReader, printer);

        predefinedToConsoleReplacer.replace();
        fileToConsoleReplacer.replace();
        consoleOnlyReplacer.replace();
    }
}

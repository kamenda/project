package org.komenda.project.printers;

public class ConsolePrinter implements Printer {
    public void print(String newString) {
        System.out.println(newString);
    }
}

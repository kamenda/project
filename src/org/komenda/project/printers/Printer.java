package org.komenda.project.printers;

public interface Printer {
    void print(String newString);
}

package org.komenda.project.printers;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Класс для печати результата в файл
 */
public class FilePrinter implements Printer {
    private String file;

    FilePrinter(String file) {
        this.file = file;
    }

    @Override
    public void print(String newString) {
        try {
            new BufferedWriter(new FileWriter(file)).write(newString);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
